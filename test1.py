
from os import listdir
from os.path import isfile, join
import numpy as np


from sklearn.linear_model import LogisticRegression

from munkres import Munkres

from time import clock
clock()



# Compute the loss for one user (i.e., one set of circles for one ego-network)
# usersPerCircle: list of sets of users (groundtruth). Order doesn't matter.
# usersPerCircleP: list of sets of users (predicted). Order doesn't matter.
def loss1(usersPerCircle, usersPerCircleP):
  #psize: either the number of groundtruth, or the number of predicted circles (whichever is larger)
  psize = max(len(usersPerCircle),len(usersPerCircleP)) # Pad the matrix to be square
  # mm: matching matrix containing costs of matching groundtruth circles to predicted circles.
  #     mm[i][j] = cost of matching groundtruth circle i to predicted circle j
  mm = np.zeros((psize,psize))
  # mm2: copy of mm since the Munkres library destroys the data during computation
  mm2 = np.zeros((psize,psize))
  for i in range(psize):
    for j in range(psize):
      circleP = set() # Match to an empty circle (delete all users)
      circle = set() # Match to an empty circle (add all users)
      if (i < len(usersPerCircleP)):
        circleP = usersPerCircleP[i]
      if (j < len(usersPerCircle)):
        circle = usersPerCircle[j]
      nedits = len(circle.union(circleP)) - len(circle.intersection(circleP)) # Compute the edit distance between the two circles
      mm[i][j] = nedits
      mm2[i][j] = nedits

  if psize == 0:
    return 0 # Edge case in case there are no circles
  else:
    m = Munkres()
    #print mm2 # Print the pairwise cost matrix
    indices = m.compute(mm) # Compute the optimal alignment between predicted and groundtruth circles
    editCost = 0
    for row, column in indices:
      #print row,column # Print the optimal value selected by matching
      editCost += mm2[row][column]
    return int(editCost)


featureNames = [line[:-1] for line in open("featureList.txt")]
featureMap = {name:i for i, name in enumerate(featureNames)}

print "loading feature data for 27520 users:",


featureArray = np.full((27520, 57), -1)
for line in open("features.txt"):
    for featureData in line.split()[1:]:
        featureArray[int(line.split()[0])][featureMap[featureData.rsplit(';', 1)[0]]] = int(featureData.rsplit(';', 1)[1])

print clock()
print "loading egonets for 110 users:",

userToEgoIndex = {}
userEgonets = []
for index, filename in enumerate(listdir("egonets")):
    userData = []
    for line in open(join("egonets\\", filename)):
        alter = int(line.split(':')[0])
        userData.append(alter)
    userEgonets.append((int(filename.split('.')[0]), userData))
    
    userToEgoIndex[int(filename.split('.')[0])] = index

print clock()
print "loading circles for 60 users:",

userToCircleIndex = {}
userCircles = []
for index, filename in enumerate(listdir("Training")):
    userData = []
    for i, line in enumerate(open(join("Training\\", filename))):
        userData.append([int(x) for x in line.split()[1:]])
    userCircles.append((int(filename.split('.')[0]), userData))
    
    userToCircleIndex[int(filename.split('.')[0])] = index

print clock()
print "fitting logistic regression to the training circles:",

distanceVectors = []
for id, egonet in userEgonets:
    egoVectors = []
    for alter in egonet:
        egoVectors.append([int(featureArray[id][i] != -1 and featureArray[alter][i] != -1 and featureArray[id][i] == featureArray[alter][i]) for i in xrange(57)])
    distanceVectors.append(egoVectors)
    
        
regs = []
for id, circles in userCircles[:30]:
    X = distanceVectors[userToEgoIndex[id]]
    _, egonet = userEgonets[userToEgoIndex[id]]
    for circle in circles:
        circleSet = set(circle)
        y = []
        for alter in egonet:
            y.append(int(alter in circleSet))
        logReg = LogisticRegression().fit(X, y)
        regs.append(logReg)


print clock()
print "combining circles:",

regError = []
minVal = float("inf")
minIndex = -1
asdf = []
for regIndex, reg in enumerate(regs):
    errorScore = []
    for index, reg2 in enumerate(regs):
        error = np.vdot(reg.coef_-reg2.coef_, reg.coef_-reg2.coef_)
        if index == regIndex:
            error = float("inf")
        elif error < minVal:
            minVal = error
            minIndex = regIndex
        errorScore.append(error)
        asdf.append(error)
    regError.append(errorScore)

clusters = []
while minVal < 9.39:
    cluster = [minIndex]
    for index, error in enumerate(regError[minIndex]):
        if error < 9.39:
            cluster.append(index)
    for index in cluster:
        for i in xrange(len(regError)):
            regError[i][index] = float("inf")
            regError[index][i] = float("inf")
    clusters.append(cluster)
    
    minVal = float("inf")
    minIndex = -1
    for index, errors in enumerate(regError):
        if min(errors) < minVal:
            minVal = min(errors)
            minIndex = index
finalReg = []
for cluster in clusters:
    coef = np.mean([regs[c].coef_ for c in cluster], axis = 0)
    regs[cluster[0]].coef_ = np.array(coef)
    finalReg.append(regs[cluster[0]])


print clock()
print "calculating loss:",

allInOneLoss = 0

for id, circles in userCircles[:30]:
    X = distanceVectors[userToEgoIndex[id]]
    _, egonet = userEgonets[userToEgoIndex[id]]
    predictedCircles = [set(egonet)]
    send = [set(circle) for circle in circles]
    allInOneLoss += loss1(send, predictedCircles)

totalLoss = 0

for id, circles in userCircles[30:]:
    X = distanceVectors[userToEgoIndex[id]]
    _, egonet = userEgonets[userToEgoIndex[id]]
    predictedCircles = []
    
    for reg in finalReg:
        pred = reg.predict(X)
        circleSet = set()
        for i, alter in enumerate(egonet):
            if pred[i]:
                circleSet.add(alter)
        if len(circleSet) > 1:
            predictedCircles.append(circleSet)
    send = [set(circle) for circle in circles]
    l = loss1(send, predictedCircles)
    totalLoss += l

print clock()
print 'Loss for "All friends in one circle":', allInOneLoss
print 'Loss for this algorithm:', totalLoss
print 'Total running time:', clock()

print "END"

