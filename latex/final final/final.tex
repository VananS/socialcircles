%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Example Paper for Penn Machine Learning - Based on ICML 2014 template %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% If you rely on Latex2e packages, like most modern people use this:
\documentclass{article}

% use Times
\usepackage{times}
% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
\usepackage{subfigure} 

% For citations
\usepackage{natbib}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

% As of 2011, we use the hyperref package to produce hyperlinks in the
% resulting PDF.  If this breaks your system, please commend out the
% following usepackage line and replace \usepackage{icml2014} with
% \usepackage[nohyperref]{icml2014} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Employ the following version of the ``usepackage'' statement for
% submitting the draft version of the paper for review.  This will set
% the note in the first column to ``Under review.  Do not distribute.''
\usepackage{pennML2015} 


% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Farabaugh, Saravanan, Yim}


\begin{document} 

\twocolumn[
\icmltitle{Learning Social Circles}

% It is OKAY to include author information, even for blind
% submissions: the style file will automatically remove it for you
% unless you've provided the [accepted] option to the icml2014
% package.
\icmlauthor{Adam Farabaugh}{alasar@seas.upenn.edu}
\icmlauthor{Alagiavanan Saravanan}{adamfara@seas.upenn.edu}
\icmlauthor{Kevin Yim}{kyim@seas.upenn.edu}

% You may provide any keywords that you 
% find helpful for describing your paper; these are used to populate 
% the "keywords" metadata in the PDF but will not be shown in the document
\icmlkeywords{social circles, machine learning}

%\vskip 0.3in
]



\begin{abstract} 
	We address the problem of determining social circles in social networks using machine learning.  The problem is described by both featureless graph data, using the friendship between users, as well as feature information on users in social networks, such as location and education.  We seek an efficient solution that will generalize well to new data.  We discuss our methods, attempting to use the user data and the graph structure to predict labels of social circles in social networks, both individually and together.  We discuss the performance metrics we use to gauge the effectiveness of our algorithms, discuss the performance of our approaches, and suggest some areas for future work.  
\end{abstract} 



\section{The Problem} 

Social circles represent an organization of one's friends by contexts in which they are known (family, friends, colleagues, etc). This organization presents a systematic classification of relationships, as well as a representation of communities and sub-communities of related members. 

Most social networking websites allow uses to create and maintain social circles manually. This is a laborious process, and even if a user does this painstakingly on creation of an account, they probably will not keep the labeling up-to-date as their social network grows.  Our project aims to automate this process by applying machine learning to the task of determining social circles in a social network.

The applications of such a capability include increasing the quality of social network services, by augmenting the information users see on usage to include more information about other users which are in shared social circles, or by facilitating social exploration by connecting users with other users outside of their immediate social circles but related through chains of social circles; For instance, it could help users make like-minded friends who have similar interests but are from different cultures and live miles apart. It could aid in predicting the automatic list of friends whom each of your posts must be shared with, as each of the posts might have content that might interest different circle of friends. High-quality social circle labeling capability could also aid monetization models, by targeting advertisements for events or products to the social circles of users who have already purchased tickets to those events, or consumed those products. 

\begin{figure}
	\centering
	\includegraphics[width=3in]{stanfordgraphic.pdf}
	\caption{Social Network Graph}
	\label{fig:stanford}
\end{figure}

\section{Survey of Current Approaches}
Most current approaches to this problem focus on using the structure of the social network to recognize communities, or use nodal information, but rarely use both together.  

Network graph analysis in the context of this problem essentially reduces to a partitioning problem.  Flow-based clustering schemes like spectral clustering are popular methods used to generate partitions. While these methods allow for flexibility in determining the similarity metric between nodes, they requires the number of clusters to be known beforehand, which is often not in the spirit of this problem.  A typical work-around is the use of training data and grid-search methods to learn a predictor for the number of clusters, but this adds execution time and complexity to the learning process, in addition to increasing the probability of overfitting.  

McAuley and Leskovec present one of the rare social network analysis algorithms that uses both structure and features, an unsupervised method to learn social circles by using gradient descent to produce the circles which most closely predict the edges in the graph \cite{leskovec}.  In particular they use the user data to determine the likelihood of an edge between two nodes with respect to a given circle.  

\section{Our Approaches}

We seek to create a social circle prediction algorithm that will utilize both structure and feature data, while still being computationally efficient, scalable, and generalizable.  

\subsection{The Data}

We obtained a dataset for this project from Kaggle.com \cite{kaggle}.  They collected this data from a user survey, which collected a user's friends' profile information from Facebook and then asked the users to hand-label the social circles in their "ego-networks", or egonets.  

The training data consists of 110 egonets, 60 of which have ground-truth labeled circles. Each user is associated with their profile data (57 features which may or may not be populated) as well as the friendship edges that connect users in a given egonet.

The feature data from Facebook is drawn directly from user profiles, and thus the quality varies a lot across users - some users fill out all their personal information, while others who may be uncomfortable giving out so much information do not. While there were 57 available features, the average number of features filled out was only 16.7, with some users filling out as few as 5 features.  No users filled out all 57 features.  Additionally, some of the more common features (less sparsely populated in our dataset) are largely irrelevant to the problem of determining social circles, such as first name and birthday.

The issue sparse and low-quality data is interrelated with the motivation for the circle prediction problem, which exists mostly because of the difficulty in convincing users to keep their social information populated and up-to-date.

Some summary statistics about the dataset are listed in \autoref{tab:data-summary}, including summary statistics of the hand-labeled ground-truth circles.  

\begin{table}[t]
	\centering
	\begin{tabular}{|l|r l|} \hline
		Number of Ego-networks & 60  & networks \\ \hline
		Mean Ego-network Size  & 242 & people   \\ \hline 
		Min Ego-network Size   & 45  & people   \\ \hline
		Max Ego-network size   & 670 & people   \\ \hline
		Mean Number of Circles & 9.9 & circles  \\ \hline
		Min Number of Circles  & 3   & circles  \\ \hline
		Max Number of Circles  & 28  & circles  \\ \hline
		Mean Circle Size       & 28.9& people   \\ \hline
		Min Circle Size        & 5   & people   \\ \hline
		Max Circle size        & 335 & people   \\ \hline
		Mean Circle Percent    & 12.2& \%      \\ \hline
		Min Circle Percent     & 0.7 & \%      \\ \hline
		Max Circle Percent     & 87.5& \%      \\ \hline
	\end{tabular}
	\caption{Ground Truth Data Summary}
	\label{tab:data-summary}
\end{table}

%mean egonet 241.983333333
%min egonet 45
%max egonet 670
%mean num circles 9.86666666667
%min num circles 3
%max num circles 28
%mean circle 28.910472973
%min circle 5
%max circle 335
%mean circle percent 0.122119184177
%min circle percent 0.00746268656716
%max circle percent 0.875

\subsection{Baseline Performance Comparisons}

We evaluate the performance of our approaches against three na\"{\i}ve baseline solutions.  The first and most na\"{\i}ve baseline is simply returning no circles for each ego.  The resulting loss is not actually a maximum-loss solution, but it does provide interesting information useful for performance comparison (as described below in \autoref{ssec:metrics}). The second baseline is the "all-friends-in-one-circle" approach, where we simply return all of an ego's friends in one circle. Finally, the third baseline consists of partitioning each egonet into its connected components, and predicting each component as a single circle.

\begin{table}[t]
	\centering
	\begin{tabular}{| c | c | c | c |} \hline
		& Ground & All Friends & Connected \\ 
		& Truth & In One Circle & Components \\ \hline
		Raw Loss 		& - 		& 21160 	& 19858 \\ \hline
		Total Size 		& - 		& 14519 	& 14519 \\ \hline
		Precision 		& - 		& 0.360 	& 0.405 \\ \hline
		Recall 			& - 		& 0.306 	& 0.344 \\ \hline
		F1 Score 		& - 		& 0.331 	& 0.372 \\ \hline
		Mean Circle		& 28.9		& 242.0 	& 22.2 	\\ \hline
		Min Circle 		& 5			& 45		& 3 	\\ \hline
		Max Circle 		& 335		& 670 		& 346 	\\ \hline
		Mean Circle	& 12.2\% 	& 100\% 	& 8.28\%\\ \hline
		Min Circle 		& 0.7\% 	& 100\% 	& 0.15\%\\ \hline
		Max Circle 		& 87.5\% 	& 100\%	& 100\% 	\\ \hline
	\end{tabular}
	\caption{Ground Truth and Benchmark Results. Circle size is given in both absolute number, and relative to the size of the egonet}
	\label{tab:results1}
\end{table}

\subsection{Performance metrics}
\label{ssec:metrics}
The Kaggle competition \cite{kaggle} provided a scoring metric which counted the minimum number of edits (node additions and subtractions) between the true circles and the predicted circles, assigning circle correlation using the Hungarian algorithm. If the number of true and predicted circles did not align, the left out circles would be counted as entirely added or entirely removed. This function allows us to compute a loss for any solution to the circles problem.  We can also calculate the size of a solution, or the total number of predictions it made - both true positive predictions (which do not need to be edited), and false positive predictions (which need to be removed).  

The loss for the "no circles" baseline has no true prediction, and thus consists entirely of the additions that need to be made.  We can then reason that the loss for any other algorithm is the no-circles loss minus the true positives, plus the false positives.  This is a system of equations:
\[size = TP + FP\]
\[loss = FP + (nocirclesloss - TP)\]
Where $size$ is the total number of predictions, $loss$ is calculated as above, and $nocirclesloss$ is the loss of the "no circles" baseline.

We can solve this system for TP and FP, yielding:
\[TP = (size + nocirclesloss - loss)/2\]
\[FP = size - TP\]

Finally, we can use $TP$, $FP$, and $nocirclesloss$ to calculate precision, recall, and F$_1$ score for each algorithm:
\[precision = TP/size\]
\[recall = TP/nocirclesloss\]
\[F_1 = 2*precision*recall/(precision+recall)\]

We cannot compute accuracy because our problem does not have a straightforward concept of false negatives, so we are limited to the above metrics. 

These metrics have been computed for the baselines and the results are tabulated in \autoref{tab:results1}.  From these and later results we will see that F$_1$ score is a rather reasonable single-valued metric to use for evaluation.  The connected-components baseline is the best-performing baseline with an F$_1$ score of $0.37$.  

\subsection{Modeling Individual Circles}

Our first approach trained a classification model for each circle in the training data based only on the users' features.  We tried multiple base classifiers, including logistic regressors and decision tree classifiers.  For each training circle in each training egonet, we computed similarity vectors in the feature space between circle members and the ego, then learned which feature(s) were good predictors of the circle.  

We tried multiple methods of choosing which models to generalize on the test data, including combining models into an ensemble if they had similar feature-importance coefficients, and selecting models which emphasized features which contributed to the most circles.  

We used half of the ground-truth dataset to train our regressors, and tested the ensemble on the other half.  See the results tabulated in \autoref{tab:results2}.  The total size of the predictions is rather small even after a significant amount of tuning, meaning that the regressors did not find enough similarity to from many of the circles that should have been predicted.  This leads to a high precision but poor recall, which is reflected in a poor F1 score as expected.  

Ultimately this approach gave poor results.  We hypothesize that this is because the feature data is not a good predictor of social circles - not inherently, but because it is self-reported and the degree of self-reporting varies too widely among users.  We also hypothesize that not using the information about friendship is not a good approach.  

\subsection{Predicting Circles Through Clustering}
%http://micans.org/mcl/index.html?sec_thesisetc
While the first method focused entirely on the feature data, the second focuses on the structure of the friendship networks.  Where before we used regressors to predict memberships, here we use a clustering approach to group alters into circles.  

For this we use the Markov Clustering Algorithm (MCL), a graph clustering algorithm.  Dongen presents this algorithm in various reports \cite{van1998new} \cite{van2000stochastic} \cite{dongen2000performance}.  

\begin{algorithm}[h]
	\caption{Markov Clustering Algorithm}
	\label{alg:mcl}
	\begin{algorithmic}
		\STATE {\bfseries Input:} graph $\mathbf{G}$, inflation operator $\Gamma$
		\STATE Add self-loops to all nodes in $\mathbf{G}$
		\STATE Initialize $\mathbf{M}_{ij}$ \COMMENT{the adjacency matrix of $\mathbf{G}$}
		\REPEAT
		\STATE $\mathbf{M}_{ij,E} = \mathbf{M}_{ij}*\mathbf{M}_{ij}$ \COMMENT{expansion}
		\STATE $\mathbf{M}_{ij} = \Gamma\left( \mathbf{M}_{ij,E} \right)$ \COMMENT{inflation}
		\STATE $Difference = $ abs$(\mathbf{M}_{ij,E} - \mathbf{M}_{ij})$
		\UNTIL{$Difference$ is $\emptyset$}
	\end{algorithmic}
\end{algorithm}

The underpinnings of MCL are probabilistic: random walks on a graph are more likely to stay within a cluster than not; in other words, it is less probable that random walks on the graph go between clusters.

The MCL algorithm starts off with adding self-looping edges on each node of the adjacency matrix; this overcomes the problem of the transition probabilities in the matrix being dependent on the case of whether path lengths are odd or even. It then simulates random walks by taking powers of the graph adjacency matrix(expansion), which in effect simulates computing very long random walks.  It then use a process called inflation, which emphasizes random walks which are more probable.  

The expansion and inflation sequence is repeated until convergence.  The algorithm carries no guarantee of convergence.  In practice, our convergence criterion compares the current inflated matrix to the last inflated matrix \textit{and} the one from two iterations previous.  This eliminates problems with small oscillations, and in practice we found convergence to run in quadratic time with graph size.  

The algorithm pseudocode is notated in algorithm \autoref{alg:mcl}, see Dongen's papers for more details.  The output is a homogeneous adjacency matrix, and the clusters are simply the connected components of the respective graph.  

MCL consists almost entirely of vectorizable operations (either multiplications or columnwise normalizations) and thus is computationally efficient when paired with accelerated linear algebra packages like BLAS.  

We first used the unweighted edges between nodes to predict circles for each egonet directly using Markov Clustering, ignoring the feature data entirely. After partitioning the graph into clusters, it predicted a circle for each cluster of nondegenerate size. This method performed significantly better than any of our previous attempts.

Finally, we augmented the clustering algorithm to weight the edges between nodes using the similarity of their feature data. For each feature that was shared between the two nodes, the weight of that edge was increased by a constant amount. Then the clustering was performed to generate circles as before. This method further increased the performance of our algorithm in predicting circles in egonets.

\section{Results}

\begin{table}[t]
	\centering
	\begin{tabular}{| c | c | c | c | c |} \hline
		& Ground & Individual & Featureless & Featured \\ 
		& Truth & Circles & Clustering & Clustering \\ \hline
		Raw Loss 	& -	& 9161 		& 14097 & 13577 \\ \hline
		Total Size 	& -	& 1051 		& 13804 & 13614 \\ \hline
		Precision 	& -	& 0.638 	& 0.609 & 0.629 \\ \hline
		Recall 		& -	& 0.071 	& 0.491 & 0.501 \\ \hline
		F1 Score 	& -	& 0.128 	& 0.544 & 0.558 \\ \hline
		Mean Circle	& 28.9	& 13.8 		& 22.2 	& 23 	\\ \hline
		Min Circle 	& 5	& 2 		& 3 	& 3 	\\ \hline
		Max Circle 	& 335	& 147 		& 346 	& 356 	\\ \hline
		Mean Circle	& 12.2\%	& 5.95\% 	& 9.15\%& 9.37\%\\ \hline
		Min Circle 	& 0.7\%	& 0.45\% 	& 0.45\%& 0.45\%\\ \hline
		Max Circle 	& 87.5\%	& 27.68\%	& 87\% 	& 71\% 	\\ \hline
	\end{tabular}
	\caption{Algorithm Results.  Note that the Individual Circle regressor ensemble was trained on half of the dataset, and tested on the other half, so we consider its raw loss to be worse than that of the clustering algorithms even though it is smaller.}
	\label{tab:results2}
\end{table}

A summary of the aforementioned performance metrics of our methods are presented in \autoref{tab:results2}.  For all the methods we tried, recall is significantly lower than precision, lending credence to our hypothesis that there is not enough information in the data to make very accurate predictions.  

We determined that using the connectivity of the nodes proved the most reliable method of predicting social circles in user egonets. Although the feature data represented avenues by which friends could be connected, the sparseness of the available data made this information less helpful than we had believed.  Out of 57 possible features, the minimum number of filled features is 5, and the maximum is 45 - the feature data specifically is very sparse.  Our best method was achieved by relying strongly on the graph structure and only augmenting our decision with the user feature data.  

\begin{figure}
	\centering
	\includegraphics[width=2.5in,trim=80 60 80 60,clip]{figure_1}
	\caption{Predicted circles, successful}
	\label{fig:one}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=2.5in,trim=80 60 80 60,clip]{figure_2}
	\caption{Ground Truth, successful}
	\label{fig:two}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=2.5in,trim=60 60 80 60,clip]{figure_5}
	\caption{Predicted circles, unsuccessful}
	\label{fig:three}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=2.5in,trim=60 60 80 60,clip]{figure_6}
	\caption{Ground Truth, unsuccessful}
	\label{fig:four}
\end{figure}

\subsection{Specific Results}
\label{ssec:specificresults}

We visualized the results of our clustering using NetworkX and Matplotlib.  In the following figures, dark red nodes do not belong to any circle, while blue, green, yellow, and orange nodes belong to circles of their respective color.  The colorings are not intended to match between the predictions and ground truth; the algorithms only predict circle structure, not assignment or correlation.  The drawn edges are friendship edges.  

An example successful clustering is shown in \autoref{fig:one} and \autoref{fig:two}.  In this egonet, the ego's hand-labeled circles agree largely with the edge-structure of the graph, and thus our clustering which emphasizes this structure heavily was able to reproduce many of the cirlces in the egonet - this particular egonet produced an F$_1$ score of 0.736.  

Most of the errors in this prediction can be seen where the algorithm predicted a single circle corresponding to a closely linked cluster of nodes, while the ground truth contains two or more clusters in the same region, or had many interior nodes that were labeled differently.

Not all egonets could be partitioned into reasonable circles.  An example of unsuccessful clustering is shown in \autoref{fig:three} and \autoref{fig:four}.  The ground truth shows a very sparse egonet with many nodes that do not belong to any circles at all. However, our algorithm attempted to predict circles and performed very poorly as a result. This result produced an F1 score of 0.2912.




%Loss for "All friends in one circle": 21160
% Size: 14519
% Precision: 0.360217645843
% Recall: 0.305830068417
% F1: 0.330803289058
%mean circle 242.0
%min circle 45
%max circle 670
%mean percent circle 100
%min percent circle 100
%max percent circle 100

%Loss for connected components: 19858
% Size: 14519
% Precision: 0.40505544459
% Recall: 0.34389801766
% F1: 0.371979759646
%mean circle 20.0262068966
%min circle 1
%max circle 630
%mean percent circle 0.0827586206897
%min percent circle 0.00149253731343
%max percent circle 1.0



\section{Conclusions and Future Work}

The most egregious assumption made by our final method is that of a flat network, i.e. that no users belong to more than one circle in an egonet.  This assumption is very limiting, and although we believe our final method is successful, we would like to explore extending it to include hierarchically structured circles in the predictions.  We considered reiterating MCL on the largest circles in the prediction set in order to recover some substructure, but this would be a heuristic (driven by the number of user in a predicted circle, for example) and may not generalize.  

Additionally, because the algorithm attempts to partition the graph into clusters, it further assumes that every node with a high connectivity is in a circle. As mentioned previously in \autoref{ssec:specificresults}, egonets may contain many users that are not part of any labeled circle at all.

Besides splitting large circles into subcircles, there are other heuristics that may have improved our raw loss performances.  However, this does not address the problem that the user data provides very little information to the problem, due to how poorly it is populated. Without more detailed data, it is difficult to determine circles in situations where the graph structure does not suffice.  Ultimately we chose not to add such heuristics.  We believe that the resulting method is a computationally efficient and generalizable tool for predicting social circles in social networks.  


\bibliography{final}
\bibliographystyle{icml2014}

%the latest algorithm
%Loss for this algorithm: 13577
% Size: 13614
% Precision: 0.629425591303
% Recall: 0.501081808081
% F1: 0.557968419339
%Total running time: 252.436344432
%mean circle 22.957841484
%min circle 3
%max circle 356
%mean percent circle 0.0936734240298
%min percent circle 0.0044776119403
%max percent circle 0.71


%the first algorithm
%Loss for this algorithm: 9161 <-only scored on half the circles (because its supervised)
% Size: 1051
% Precision: 0.638439581351
% Recall: 0.0709902666102
% F1: 0.127773017233
%Total running time: 15.8193196182
%mean circle 13.8289473684
%min circle 2
%max circle 147
%mean percent circle 0.0595031346034
%min percent circle 0.00447427293065
%max percent circle 0.276836158192
 

%featureless clustering
%Loss for this algorithm: 14097
% Size: 13804
% Precision: 0.608809040858
% Recall: 0.491433249518
% F1: 0.543860216793
%Total running time: 182.585182204
%mean circle 22.192926045
%min circle 3
%max circle 346
%mean percent circle 0.0915463236602
%min percent circle 0.0044776119403
%max percent circle 0.87


\end{document} 


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was
% created by Lise Getoor and Tobias Scheffer, it was slightly modified  
% from the 2010 version by Thorsten Joachims & Johannes Fuernkranz, 
% slightly modified from the 2009 version by Kiri Wagstaff and 
% Sam Roweis's 2008 version, which is slightly modified from 
% Prasad Tadepalli's 2007 version which is a lightly 
% changed version of the previous year's version by Andrew Moore, 
% which was in turn edited from those of Kristian Kersting and 
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.  
