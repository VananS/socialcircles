%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Example Paper for Penn Machine Learning - Based on ICML 2014 template %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% If you rely on Latex2e packages, like most modern people use this:
\documentclass{article}

% use Times
\usepackage{times}
% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
\usepackage{subfigure} 

% For citations
\usepackage{natbib}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

% As of 2011, we use the hyperref package to produce hyperlinks in the
% resulting PDF.  If this breaks your system, please commend out the
% following usepackage line and replace \usepackage{icml2014} with
% \usepackage[nohyperref]{icml2014} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Employ the following version of the ``usepackage'' statement for
% submitting the draft version of the paper for review.  This will set
% the note in the first column to ``Under review.  Do not distribute.''
\usepackage{pennML2015} 


% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Farabaugh, Saravanan, Yim}


\begin{document} 

\twocolumn[
\icmltitle{Project Report Template for CIS 419/519\\Introduction to Machine Learning}

% It is OKAY to include author information, even for blind
% submissions: the style file will automatically remove it for you
% unless you've provided the [accepted] option to the icml2014
% package.
\icmlauthor{Adam Farabaugh}{alasar@seas.upenn.edu}
\icmlauthor{Alagiavanan Saravanan}{adamfara@seas.upenn.edu}
\icmlauthor{Kevin Yim}{kyim@seas.upenn.edu}

% You may provide any keywords that you 
% find helpful for describing your paper; these are used to populate 
% the "keywords" metadata in the PDF but will not be shown in the document
\icmlkeywords{social circles, machine learning}

%\vskip 0.3in
]

\begin{abstract} 
	Our project addressed the problem of determining social circles in social networks, using user information for each node. We discuss our methods and the performance metrics we used to gauge the effectiveness of our algorithms.  
\end{abstract} 


\section{The Problem} 
Social circles represent an organization of one's friends by contexts in which they are known (family, friends, colleagues, etc). This organization presents a systematic classification of relationships, as well as a representation of communities and sub-communities of related members. 

Most social networking websites allow uses to create and maintain social circles manually. This takes time and effort to establish and maintain. Our project aims to automate this process by applying machine learning to the task of determining social circles in a social network.

\section{Survey of Current Approaches}
Most current approaches to this problem focus on using the structure of the graph to determine communities in the network. These solutions do not use other user information, such as school or location.

McAuley and Leskovec discuss an unsupervised method to learn social circles by using gradient descent to produce the circles which most closely predict the edges in the graph\yrcite{leskovec}
.  In particular they use the user data to determine the likelihood of an edge between two nodes with respect to a given circle.

\section{Our Approaches}
\subsection{The Data}

We obtained a dataset for this project from Kaggle.com\footnote{\url{https://www.kaggle.com/c/learning-social-circles}}
.  They collected this data from a user survey, which grabbed friend information from Facebook and then asked the users to hand-label the social circles in their "ego-networks", or egonets.  

The training data consists of a map from features to users (58 features which may or may not be populated) alongside a map from an ego to a list of their friends.  The dataset also contains test data which consists of a map from an ego to a set of their hand-labeled social circles, which are in turn sets of users.  

The feature data from Facebook is drawn directly from user profiles, and thus the quality varies a lot across users - some users fill out all their personal information, while others who may be uncomfortable giving out so much information do not.  

The circle labels are of fairly high quality, but also show a lot of variation.  Some "egos" label only a few large social circles, while others label many smaller social circles.  

The variation in the data makes the problem difficult, but also realistic.  

%\begin{table}
%	\centering
%	
%	\begin{tabular}{|l|r l|} \hline
%		Number of Ego-networks & 60  & networks \\ \hline
%		Mean Ego-network Size  & 242 & people   \\ \hline 
%		Min Ego-network Size   & 45  & people   \\ \hline
%		Max Ego-network size   & 670 & people   \\ \hline
%		Mean Number of Circles & 9.9 & circles  \\ \hline
%		Min Number of Circles  & 3   & circles  \\ \hline
%		Max Number of Circles  & 28  & circles  \\ \hline
%		Mean Circle Size       & 28.9& people   \\ \hline
%		Min Circle Size        & 5   & people   \\ \hline
%		Max Circle size        & 335 & people   \\ \hline
%		Mean Circle Percent    & 12.2& \%      \\ \hline
%		Min Circle Percent     & 0.7 & \%      \\ \hline
%		Max Circle Percent     & 87.5& \%      \\ \hline
%	\end{tabular}
%	\caption{Ground Truth Data Summary}
%	\label{tab:data-summary}
%\end{table}

%mean egonet 241.983333333
%min egonet 45
%max egonet 670
%mean num circles 9.86666666667
%min num circles 3
%max num circles 28
%mean circle 28.910472973
%min circle 5
%max circle 335
%mean circle percent 0.122119184177
%min circle percent 0.00746268656716
%max circle percent 0.875


\begin{table*}[t]
	\centering
	\begin{tabular}{| c | c | c | c |} \hline
		& Individual Circles & Featureless Clustering & Clustering with Features \\ \hline
		Raw Loss & 9161 & 14097 & 13577 \\ \hline
		Total Size & 1051 & 13804 & 13614 \\ \hline
		Precision & 0.638 & 0.609 & 0.629 \\ \hline
		Recall & 0.071 & 0.491 & 0.501 \\ \hline
		F1 Score & 0.128 & 0.544 & 0.558 \\ \hline
		%Mean Circle Size & 13.8 & 22.2 & 23 \\ \hline
		%Min Circle Size & 2 & 3 & 3 \\ \hline
		%Max Circle Size & 147 & 346 & 356 \\ \hline
		%Mean Circle Percent & 5.95\% & 9.15\% & 9.37\% \\ \hline
		%Min Circle Percent & 0.45\% & 0.45\% & 0.45\% \\ \hline
		%Max Circle Percent & 27.68\%& 87\% & 71\% \\ \hline
	\end{tabular}
	\caption{Algorithm Results}
	\label{tab:results}
\end{table*}

\subsection{Baseline Performance}

We evaluate the performance of our approaches against two na\"{\i}ve baseline solutions.  The first and most na\"{\i}ve baseline is simply returning no circles for each ego.  The resulting loss is not actually a maximum-loss solution, but it does provide interesting information useful for performance comparison (as described below in \autoref{ssec:metrics}). The second baseline is the "all-friends-in-one-circle" approach, where we simply return all of an ego's friends in one circle.  


\subsection{Performance metrics}
\label{ssec:metrics}
The Kaggle competition provided a scoring metric which counted the minimum number of edits (addition and subtraction) between the true circles and the predicted circles, assigning circle correlation using the Hungarian algorithm.  This function allows us to compute a loss for any solution to the circles problem.  We can also calculate the size of a solution, or the total number of predictions it made - both true positive predictions (which do not need to be edited), and false positive predictions (which need to be edited, namely, removed).  

The loss for the "no circles" baseline has no true prediction, and thus consists entirely of the additions that need to be made.  We can then reason that the loss for any other algorithm is the no-circles loss minus the true positives, plus the false positives.  This is a system of equations:
\[size = TP + FP\]
\[loss = FP + (nocirclesloss - TP)\]
Solving this system for each approach, we can calculate precision, recall, and F1 score of using the total number of nodes in the true circles and the total number of nodes in the predicted circles. We used these values to evaluate the performance of our algorithms.  

We cannot compute accuracy because our problem does not have a concept of true negatives, so we are limited to the above metrics.  

\subsection{Modeling Individual Circles}

Our first approach trained a classification model for each circle in the training data based only on the users' features.  We tried multiple base classifiers, including logistic regressors and decision tree classifiers.  For each circle in each egonet, we computed similarity vectors in the feature space between circle members and the ego, then learned which feature(s) were good predictors of the circle.  

We tried multiple methods of choosing which models to generalize on the test data, including combining models into an ensemble if they had similar feature-importance coefficients, and selecting models which emphasized features which contributed to the most circles.  

Ultimately this approach gave poor results.  We hypothesize that this is because the feature data is not a good predictor of social circles - not inherently, but because it is self-reported and the degree of self-reporting varies too widely among users.  We also hypothesize that not using the information about friendship is not a good approach.  

\subsection{Predicting Circles Through Clustering}
%http://micans.org/mcl/index.html?sec_thesisetc

Our second approach used the edges between nodes to predict circles for each egonet. Our very first attempt used an unsupervised algorithm which performed the Markov Clustering algorithm to find node clusters on each egonet, ignoring the feature data entirely. This method performed significantly better than any of our previous attempts.

Finally, we augmented the clustering algorithm to weight the edges between nodes using the similarity of their feature data. For each feature that was shared between the two nodes, the weight of that edge was increased by a constant amount. Then the clustering was performed to generate circles. This method further increased the performance of our algorithm in predicting circles in egonets.

\section{Results}

We determined that using the connectivity of the nodes proved the most reliable method of predicting social circles in user egonets. This corresponds with current work in this area. However, by informing this solution with the feature similarity of friends in the egonet, we were able to further improve performance.


\section{Future Work}

The dataset we used is rather sparse in the sense that connectivity between egonets is very small.  There are 7 edges between all 60 egonet graphs, while the total number of nodes in the graph is 27520.  We believe that with a less-sparse graph, clustering may work even better than it does in our current work, and different types of clustering may be more effective as well.  

We also believe that some simple heuristics could improve the effectiveness of our algorithm.  


\bibliography{final}
\bibliographystyle{icml2014}


%the latest algorithm
%Loss for this algorithm: 13577
% Size: 13614
% Precision: 0.629425591303
% Recall: 0.501081808081
% F1: 0.557968419339
%Total running time: 252.436344432
%mean circle 22.957841484
%min circle 3
%max circle 356
%mean percent circle 0.0936734240298
%min percent circle 0.0044776119403
%max percent circle 0.71
%END

%the first algorithm
%Loss for this algorithm: 9161 <-only scored on half the circles (because its supervised)
% Size: 1051
% Precision: 0.638439581351
% Recall: 0.0709902666102
% F1: 0.127773017233
%Total running time: 15.8193196182
%mean circle 13.8289473684
%min circle 2
%max circle 147
%mean percent circle 0.0595031346034
%min percent circle 0.00447427293065
%max percent circle 0.276836158192
%END 

%featureless clustering
%Loss for this algorithm: 14097
% Size: 13804
% Precision: 0.608809040858
% Recall: 0.491433249518
% F1: 0.543860216793
%Total running time: 182.585182204
%mean circle 22.192926045
%min circle 3
%max circle 346
%mean percent circle 0.0915463236602
%min percent circle 0.0044776119403
%max percent circle 0.87
%END

\end{document} 


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was
% created by Lise Getoor and Tobias Scheffer, it was slightly modified  
% from the 2010 version by Thorsten Joachims & Johannes Fuernkranz, 
% slightly modified from the 2009 version by Kiri Wagstaff and 
% Sam Roweis's 2008 version, which is slightly modified from 
% Prasad Tadepalli's 2007 version which is a lightly 
% changed version of the previous year's version by Andrew Moore, 
% which was in turn edited from those of Kristian Kersting and 
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.  
