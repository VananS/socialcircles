
from os import listdir
from os.path import isfile, join
import numpy as np
import json
import pickle

from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from munkres import Munkres

from time import clock
clock()


# Compute the loss for one user (i.e., one set of circles for one ego-network)
# usersPerCircle: list of sets of users (groundtruth). Order doesn't matter.
# usersPerCircleP: list of sets of users (predicted). Order doesn't matter.
def loss1(usersPerCircle, usersPerCircleP):
  #psize: either the number of groundtruth, or the number of predicted circles (whichever is larger)
  psize = max(len(usersPerCircle),len(usersPerCircleP)) # Pad the matrix to be square
  # mm: matching matrix containing costs of matching groundtruth circles to predicted circles.
  #     mm[i][j] = cost of matching groundtruth circle i to predicted circle j
  mm = np.zeros((psize,psize))
  # mm2: copy of mm since the Munkres library destroys the data during computation
  mm2 = np.zeros((psize,psize))
  for i in range(psize):
    for j in range(psize):
      circleP = set() # Match to an empty circle (delete all users)
      circle = set() # Match to an empty circle (add all users)
      if (i < len(usersPerCircleP)):
        circleP = usersPerCircleP[i]
      if (j < len(usersPerCircle)):
        circle = usersPerCircle[j]
      nedits = len(circle.union(circleP)) - len(circle.intersection(circleP)) # Compute the edit distance between the two circles
      mm[i][j] = nedits
      mm2[i][j] = nedits

  if psize == 0:
    return 0 # Edge case in case there are no circles
  else:
    m = Munkres()
    #print mm2 # Print the pairwise cost matrix
    indices = m.compute(mm) # Compute the optimal alignment between predicted and groundtruth circles
    editCost = 0
    for row, column in indices:
      #print row,column # Print the optimal value selected by matching
      editCost += mm2[row][column]
    return int(editCost)

### Load all feature data to memory
# Load feature names
# A list of strings, serves as a map from feature numbers to feature names
if (isfile('featureNames.json')):
    file = open('featureNames.json')
    featureNames = json.load(file)
    file.close()
else:
    featureNames = [line[:-1] for line in open("featureList.txt")]
    file = open('featureNames.json','w+')
    json.dump(featureNames,file)
    file.close()
#end

# Enumerate names to create a map from name to number
if (isfile('featureMap.json')):
    file = open('featureMap.json')
    featureMap = json.load(file)
    file.close()
else:
    featureMap = {name:i for i, name in enumerate(featureNames)}
    file = open('featureMap.json','w+')
    json.dump(featureMap,file)
    file.close()
#end

# Load actual datapoints from file
print "loading feature data for 27520 users:",
if (isfile('featureArray.json')):
    file = open('featureArray.json')
    featureArray = np.array(json.load(file))
    file.close()
else:
    featureArray = np.full((27520, 57), -1)
    for line in open("features.txt"):
        for featureData in line.split()[1:]:
            featureArray[int(line.split()[0])][featureMap[featureData.rsplit(';', 1)[0]]] = int(featureData.rsplit(';', 1)[1])
            
    featureArray = featureArray.astype(int)
    
    file = open('featureArray.json','w+')
    json.dump(featureArray.tolist(),file)
    file.close()
#end

def getDistanceVector(trueEgo, ego, alter):
    result = [int(featureArray[ego][i] != -1 and featureArray[alter][i] != -1 and featureArray[ego][i] == featureArray[alter][i]) for i in xrange(57)]
    return result

print clock()
print "loading egonets for 60/110 users,"
print "(ignoring egonets with no circles):",

userToEgoIndex = {}
userEgonets = []
userAndAlterToEdges = {}
for index, filename0 in enumerate(listdir("Training")):
    filename = filename0.split('.')[0]+".egonet"
    userData = []
    for line in open(join("egonets", filename)):
        alter = int(line.split(':')[0])
        userData.append(alter)
        edges = []
        for edge in line.split(':')[1].split():
            edges.append(int(edge))
        userAndAlterToEdges[(int(filename.split('.')[0]), alter)] = edges
    userEgonets.append((int(filename.split('.')[0]), userData))
    
    userToEgoIndex[int(filename.split('.')[0])] = index

print clock()


addNewEdges = False
weight = .2
print clock()
print "predicting circles:",

predCircles = {}
for id, egonet in userEgonets:
    mat = np.matrix(np.zeros((len(egonet), len(egonet))))
    alterToIndex = {alter:index for index, alter in enumerate(egonet)}
    
    seen = set()
    circles = []
    
    for i, alter in enumerate(egonet):
        if alter not in seen:
            queue = [alter]
            circle = set()
            while len(queue) > 0:
                look = queue[0]
                queue = queue[1:]
                if look not in seen:
                    seen.add(look)
                    circle.add(look)
                    queue += userAndAlterToEdges[(id, look)]
            circles.append(circle)
    predCircles[id] = circles
    

print "loading circles for 60 users:",

userToCircleIndex = {}
userCircles = []
for index, filename in enumerate(listdir("Training")):
    userData = []
    for i, line in enumerate(open(join("Training", filename))):
        userData.append([int(x) for x in line.split()[1:]])
    userCircles.append((int(filename.split('.')[0]), userData))
    
    userToCircleIndex[int(filename.split('.')[0])] = index

print clock()
print "calculating loss:", 

noCirclesLoss = 0

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    predictedCircles = []
    send = [set(circle) for circle in circles]
    noCirclesLoss += loss1(send, predictedCircles)

allInOneLoss = 0
allInOneSize = 0

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    allInOneSize += len(egonet)
    predictedCircles = [set(egonet)]
    send = [set(circle) for circle in circles]
    allInOneLoss += loss1(send, predictedCircles)

allInOneYes = (allInOneSize+noCirclesLoss-allInOneLoss)/2.
allInOneNo = allInOneSize-allInOneYes

totalLoss = 0
totalSize = 0

allcircles = []
percentcircles = []

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    
    for c in predCircles[id]:
        totalSize += len(c)
    
    send = [set(circle) for circle in circles]
    l = loss1(send, predCircles[id])
    allcircles += [len(c) for c in predCircles[id]]
    percentcircles += [len(c)/float(len(egonet)) for c in predCircles[id]]
    totalLoss += l

totalYes = (totalSize+noCirclesLoss-totalLoss)/2.
totalNo = totalSize-totalYes

print clock()
print 'Loss for "No circles":', noCirclesLoss
print 'Loss for "All friends in one circle":', allInOneLoss
print ' Size:', allInOneSize
print ' Precision:', allInOneYes/allInOneSize
print ' Recall:', allInOneYes/noCirclesLoss
print ' F1:', 2*(allInOneYes/allInOneSize)*(allInOneYes/noCirclesLoss)/(allInOneYes/allInOneSize+allInOneYes/noCirclesLoss)
print 'Loss for this algorithm:', totalLoss
print ' Size:', totalSize
print ' Precision:', totalYes/totalSize
print ' Recall:', totalYes/noCirclesLoss
print ' F1:', 2*(totalYes/totalSize)*(totalYes/noCirclesLoss)/(totalYes/totalSize+totalYes/noCirclesLoss)
print 'Total running time:', clock()

print 'mean circle', np.mean(allcircles)
print 'min circle', np.min(allcircles)
print 'max circle', np.max(allcircles)
print 'mean percent circle', np.mean(percentcircles)
print 'min percent circle', np.min(percentcircles)
print 'max percent circle', np.max(percentcircles)

print "END"

