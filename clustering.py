
from os import listdir
from os.path import isfile, join
import numpy as np

from sklearn.tree import DecisionTreeClassifier

from sklearn.linear_model import LogisticRegression

from munkres import Munkres

from time import clock
clock()


timeLimit = float('inf')


# Compute the loss for one user (i.e., one set of circles for one ego-network)
# usersPerCircle: list of sets of users (groundtruth). Order doesn't matter.
# usersPerCircleP: list of sets of users (predicted). Order doesn't matter.
def loss1(usersPerCircle, usersPerCircleP):
  #psize: either the number of groundtruth, or the number of predicted circles (whichever is larger)
  psize = max(len(usersPerCircle),len(usersPerCircleP)) # Pad the matrix to be square
  # mm: matching matrix containing costs of matching groundtruth circles to predicted circles.
  #     mm[i][j] = cost of matching groundtruth circle i to predicted circle j
  mm = np.zeros((psize,psize))
  # mm2: copy of mm since the Munkres library destroys the data during computation
  mm2 = np.zeros((psize,psize))
  for i in range(psize):
    for j in range(psize):
      circleP = set() # Match to an empty circle (delete all users)
      circle = set() # Match to an empty circle (add all users)
      if (i < len(usersPerCircleP)):
        circleP = usersPerCircleP[i]
      if (j < len(usersPerCircle)):
        circle = usersPerCircle[j]
      nedits = len(circle.union(circleP)) - len(circle.intersection(circleP)) # Compute the edit distance between the two circles
      mm[i][j] = nedits
      mm2[i][j] = nedits

  if psize == 0:
    return 0 # Edge case in case there are no circles
  else:
    m = Munkres()
    #print mm2 # Print the pairwise cost matrix
    indices = m.compute(mm) # Compute the optimal alignment between predicted and groundtruth circles
    editCost = 0
    for row, column in indices:
      #print row,column # Print the optimal value selected by matching
      editCost += mm2[row][column]
    return int(editCost)

    
#featureNames = [line[:-1] for line in open("featureList.txt")]
#featureMap = {name:i for i, name in enumerate(featureNames)}
#
#print "loading feature data for 27520 users:",
#
#
#featureArray = np.full((27520, 57), -1)
#for line in open("features.txt"):
#    for featureData in line.split()[1:]:
#        featureArray[int(line.split()[0])][featureMap[featureData.rsplit(';', 1)[0]]] = int(featureData.rsplit(';', 1)[1])
#
#print clock()
print "loading egonets for 60/110 users,"
print "(ignoring egonets with no circles):",

userToEgoIndex = {}
userEgonets = []
userAndAlterToEdges = {}
for index, filename0 in enumerate(listdir("Training")):
    filename = filename0.split('.')[0]+".egonet"
    userData = []
    for line in open(join("egonets\\", filename)):
        alter = int(line.split(':')[0])
        userData.append(alter)
        edges = []
        for edge in line.split(':')[1].split():
            edges.append(int(edge))
        userAndAlterToEdges[(int(filename.split('.')[0]), alter)] = edges
    userEgonets.append((int(filename.split('.')[0]), userData))
    
    userToEgoIndex[int(filename.split('.')[0])] = index

print clock()
print "predicting circles for egonet:"

predCircles = {}
for id, egonet in userEgonets:
    print str(id)+":",
    mat = np.matrix(np.zeros((len(egonet), len(egonet))))
    alterToIndex = {alter:index for index, alter in enumerate(egonet)}
    for i, alter in enumerate(egonet):
        total = len(userAndAlterToEdges[(id, alter)])+1.
        for edge in userAndAlterToEdges[(id, alter)]:
            mat[alterToIndex[edge], i] = 1/total
        mat[i, i] = 1/total
        
    twoLast = np.zeros((1, 1))
    lastMat = np.zeros((1, 1))
    while not (np.array_equal(mat, lastMat) or np.array_equal(mat, twoLast)):
        twoLast = lastMat
        lastMat = mat
        mat = np.vectorize(lambda x: x**2)(lastMat*lastMat)
        for j in xrange(len(egonet)):
            s = np.sum(mat[:,j])
            mat[:,j] = np.vectorize(lambda x: x/float(s))(mat[:,j])
    circles = []
    seen = set()
    for i, row in enumerate(mat):
        if np.sum(row) > 0:
            circle = set()
            for j in xrange(len(egonet)):
                if mat[i, j] > 0:
                    circle.add(egonet[j])
            frozen = frozenset(circle)
            if len(circle) > 2 and (np.sum(row) == 1 or frozen not in seen):
                circles.append(circle)
                if np.sum(row) != 1:
                    seen.add(frozen)
    predCircles[id] = circles
    print clock()
    if clock() > timeLimit:
        break

print "loading circles for 60 users:",

userToCircleIndex = {}
userCircles = []
for index, filename in enumerate(listdir("Training")):
    userData = []
    for i, line in enumerate(open(join("Training\\", filename))):
        userData.append([int(x) for x in line.split()[1:]])
    userCircles.append((int(filename.split('.')[0]), userData))
    
    userToCircleIndex[int(filename.split('.')[0])] = index

print clock()
print "calculating loss:",

noCirclesLoss = 0

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    predictedCircles = []
    send = [set(circle) for circle in circles]
    noCirclesLoss += loss1(send, predictedCircles)

allInOneLoss = 0
allInOneSize = 0

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    allInOneSize += len(egonet)
    predictedCircles = [set(egonet)]
    send = [set(circle) for circle in circles]
    allInOneLoss += loss1(send, predictedCircles)

allInOneYes = (allInOneSize+noCirclesLoss-allInOneLoss)/2.
allInOneNo = allInOneSize-allInOneYes

totalLoss = 0
totalSize = 0

for id, circles in userCircles:
    if not id in predCircles:
        continue
    _, egonet = userEgonets[userToEgoIndex[id]]
    
    for c in predCircles[id]:
        totalSize += len(c)
    
    send = [set(circle) for circle in circles]
    l = loss1(send, predCircles[id])
    totalLoss += l

totalYes = (totalSize+noCirclesLoss-totalLoss)/2.
totalNo = totalSize-totalYes

print clock()
print 'Loss for "No circles":', noCirclesLoss
print 'Loss for "All friends in one circle":', allInOneLoss
print ' Size:', allInOneSize
print ' Precision:', allInOneYes/allInOneSize
print ' Recall:', allInOneYes/noCirclesLoss
print ' F1:', 2*(allInOneYes/allInOneSize)*(allInOneYes/noCirclesLoss)/(allInOneYes/allInOneSize+allInOneYes/noCirclesLoss)
print 'Loss for this algorithm:', totalLoss
print ' Size:', totalSize
print ' Precision:', totalYes/totalSize
print ' Recall:', totalYes/noCirclesLoss
print ' F1:', 2*(totalYes/totalSize)*(totalYes/noCirclesLoss)/(totalYes/totalSize+totalYes/noCirclesLoss)
print 'Total running time:', clock()

print "END"

